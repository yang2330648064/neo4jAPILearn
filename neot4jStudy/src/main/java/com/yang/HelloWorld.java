package com.yang;

import org.neo4j.driver.*;

public class HelloWorld implements AutoCloseable {
    //定义驱动
    private final Driver driver;

    public HelloWorld(String url,String username,String password) {
        //驱动初始化
        this.driver= GraphDatabase.driver(url, AuthTokens.basic(username,password));
    }

    @Override
    public void close() {
        this.driver.close();
    }

    public String createNode(String name){
        //通过驱动获取到session，通过session进行操作neo4j数据库
        try(Session session = driver.session()){
            Value id = session.writeTransaction(tx -> {
                //定义执行的语句
                String create = "create (n:User {name:'"+name+"'}) return id(n) as id";
                //执行，获取结果
                Result result = tx.run(create);

                return result.single().get("id");
            });
            return id.toString();
        }
    }

    public static void main(String[] args) throws Exception {
        String url="bolt://192.168.1.21:7687";
        String user="neo4j";
        String password="neo4jneo4j";
        try(HelloWorld helloWorld=new HelloWorld(url,user,password)) {
            System.out.println(helloWorld.createNode("李四"));
            System.out.println(helloWorld.createNode("王五"));
            System.out.println(helloWorld.createNode("赵六"));
        }
    }
}