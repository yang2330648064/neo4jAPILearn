package com.yang.ogm;

import com.yang.ogm.pojo.Movie;
import com.yang.ogm.pojo.MyPerson;
import com.yang.ogm.service.PersonService;
import com.yang.ogm.session.SessionFactoryUtils;
import org.junit.Test;
import org.neo4j.driver.internal.InternalPath;
import org.neo4j.ogm.model.Result;
import org.neo4j.ogm.session.SessionFactory;

import java.util.List;

public class TestPersonService {
    private PersonService personService=new PersonService();

    @Test
    public void getSessionFactory(){
        SessionFactory sessionFactory = SessionFactoryUtils.getSessionFactory();
        System.out.println(sessionFactory);
    }

    @Test
    public void testQueryPersonById(){
        MyPerson myPerson = this.personService.queryPersonById(33L,2);
        System.out.println(myPerson);
    }

    @Test
    public void testQueryPersonByName(){
        List<MyPerson> list = this.personService.queryPersonByName("Tom Hanks");
        System.out.println(list);
    }

    @Test
    public void testQueryPersonByCypher(){
        Result result = this.personService.queryAllShortestPathsByCypher("Susan Sarandon", "Jessica Thompson");
        result.forEach(stringObjectMap -> {
            InternalPath.SelfContainedSegment[] arrays=(InternalPath.SelfContainedSegment[]) stringObjectMap.get("p");
            for (InternalPath.SelfContainedSegment segment : arrays) {
                System.out.println(segment);
            }
        });
    }

    @Test
    public void testSavePerson(){
        MyPerson myPerson = new MyPerson();
        myPerson.setName("沈腾");
        myPerson.setBorn(1970L);

        Movie movie = new Movie();
        movie.setTitle("飞驰人生");
        movie.setReleased(2019L);


        //添加关系
        // myPerson.setMovies(List.of(movie));
        //
        // this.personService.savePerson(myPerson);
        // System.out.println(myPerson);
    }

    @Test
    public void testUpdate() {
        List<MyPerson> personList = personService.queryPersonByName("沈腾");
        MyPerson person = personList.get(0);
        person.setBorn(1980L);
        personService.savePerson(person);
    }

    @Test
    public void testDeletePersonByNmae() {
        Object result = personService.deletePersonByName("沈腾");
        System.out.println(result);
    }

}
