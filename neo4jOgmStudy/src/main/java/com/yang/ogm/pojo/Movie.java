package com.yang.ogm.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.*;

import java.util.List;

@NodeEntity
@Setter
@Getter
@NoArgsConstructor
@ToString
public class Movie {
    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private Long released;

    @Property("tagline")
    private String tagLine;

    //设置关系并且指定方向
    @Relationship(type = "ACTED_IN",direction = Relationship.Direction.INCOMING)
    private List<ActedIn> actedIns;
}
