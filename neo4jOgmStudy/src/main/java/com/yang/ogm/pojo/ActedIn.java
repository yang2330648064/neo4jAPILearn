package com.yang.ogm.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.*;

@RelationshipEntity(type = "ACTED_IN")
@Setter
@Getter
@ToString(exclude = {"person","movie"})
@NoArgsConstructor
public class ActedIn {
    @Id
    @GeneratedValue
    private Long id;

    @StartNode //关系中的开始节点
    private MyPerson person;

    @EndNode  //关系中的结束节点
    private Movie movie;

    private String[] roles;
}
