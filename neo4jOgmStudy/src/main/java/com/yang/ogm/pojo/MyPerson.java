package com.yang.ogm.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.*;

import java.util.List;

@NodeEntity(label="Person")
@Setter
@Getter
@NoArgsConstructor
@ToString
public class MyPerson {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private Long born;

    //指定关系
    //@Relationship(type = "ACTED_IN",direction = Relationship.Direction.OUTGOING)
    //private List<Movie> movies;

    @Relationship(type = "ACTED_IN")
    private List<ActedIn> actedIns;
    //导演关系
    @Relationship(type = "DIRECTED")
    private List<Movie> directedMovies;

    //出品关系
    @Relationship(type = "PRODUCED")
    private List<Movie> producedMovies;

    //写作关系
    @Relationship(type = "WROTE")
    private List<Movie> wroteMovies;
}