package com.yang.ogm.session;

import lombok.Getter;
import org.neo4j.ogm.config.Configuration;
import org.neo4j.ogm.session.SessionFactory;

public class SessionFactoryUtils {

    private static final Configuration configuration=new Configuration.Builder()
            .uri("bolt://neo4j:neo4jneo4j@192.168.1.21:7687")
            .connectionPoolSize(150)
            .connectionLivenessCheckTimeout(100)
            .build();

    @Getter
    private static SessionFactory sessionFactory=new SessionFactory(configuration,"com.yang.ogm.pojo");

}
